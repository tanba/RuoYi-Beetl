package com.ruoyi.biz.controller.api;

import com.ruoyi.common.core.bo.ResponseData;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SimpleMap;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 项目列表Controller
 *
 * @author LiuQi
 * @date 2019-10-05
 */
@Controller
@RequestMapping("/api/select")
public class ApiSelectController extends BaseController
{

    @Autowired
    private ISysUserService userService;


    @PostMapping("/user")
    @ResponseBody
    public ResponseData user()
    {
        List<SysUser> list = userService.selectUserList(new SysUser());
        List<SimpleMap> data = new ArrayList<>();
        for (SysUser info : list) {
            if(info.getUserId().intValue() == 1){
                continue;
            }
            data.add(new SimpleMap().set("name",info.getUserName()).set("value",info.getUserId()));
        }
        return SUCCESS(data);
    }

}
