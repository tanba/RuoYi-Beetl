package com.ruoyi.web.core.function;

import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.framework.web.service.PermissionService;
import com.ruoyi.web.core.anno.FunctionName;
import org.beetl.core.Context;
import org.beetl.core.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 权限验证函数
 * @author iterking
 **/
@FunctionName("principal")
@Component
public class PrincipalFunction implements Function {

    @Autowired
    private PermissionService permissionService;

    @Override
    public Object call(Object[] paras, Context ctx) {
        if(ShiroUtils.isAdmin()){
            return true;
        }
        for (Object para : paras) {
            if(para != null){
                Object val = permissionService.getPrincipalProperty(para.toString());
                try {
                    if(val != null){
                        ctx.byteWriter.writeString(val.toString());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }
}
