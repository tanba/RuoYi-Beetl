package com.ruoyi.web.core.config.beetl;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.AppCtx;
import com.ruoyi.common.utils.SimpleMap;
import com.ruoyi.web.core.anno.FunctionName;
import com.ruoyi.web.core.anno.TagName;
import org.beetl.core.Function;
import org.beetl.core.GroupTemplate;
import org.beetl.core.tag.Tag;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;

import java.util.Map;


/**
 * beetl拓展配置,绑定一些工具类,方便在模板中直接调用
 *
 * @author stylefeng
 * @Date 2018/2/22 21:03
 */
public class BeetlConfiguration extends BeetlGroupUtilConfiguration {

    private String staticHome;
    private String jsCssVersion;

    @Override
    public void initOther() {

        groupTemplate.setSharedVars(new SimpleMap<String,Object>()
                .set("staticHome",staticHome)
                .set("jsCssVersion",jsCssVersion)
        );

        groupTemplate.registerFunctionPackage("strTools", new StrUtil());

        //放置自定义标签
        Map<String, Tag> tags = AppCtx.getApplicationContext().getBeansOfType(Tag.class);
        for (String beanName : tags.keySet()) { // 读取自定义标签名
            TagName anno = tags.get(beanName).getClass().getAnnotation(TagName.class);
            String name = anno != null ? anno.value() : beanName;
            groupTemplate.registerTag(name,tags.get(beanName).getClass());
        }

        //放置自定义方法
        Map<String, Function> functions = AppCtx.getApplicationContext().getBeansOfType(Function.class);
        for (String beanName : functions.keySet()) { // 读取自定义标签名
            FunctionName anno = functions.get(beanName).getClass().getAnnotation(FunctionName.class);
            String name = anno != null ? anno.value() : beanName;
            groupTemplate.registerFunction(name,functions.get(beanName));
        }

    }

    public void setStaticHome(String staticHome) {
        this.staticHome = staticHome;
    }

    public void setJsCssVersion(String jsCssVersion) {
        this.jsCssVersion = jsCssVersion;
    }

    @Override
    public GroupTemplate getGroupTemplate() {
        return groupTemplate;
    }
}
