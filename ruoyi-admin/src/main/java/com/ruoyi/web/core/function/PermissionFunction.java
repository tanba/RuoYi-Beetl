package com.ruoyi.web.core.function;

import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.framework.web.service.PermissionService;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.web.core.anno.FunctionName;
import org.beetl.core.Context;
import org.beetl.core.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 权限验证函数
 * @author iterking
 **/
@FunctionName("permission")
@Component
public class PermissionFunction implements Function {

    @Autowired
    private PermissionService permissionService;

    @Override
    public Object call(Object[] paras, Context ctx) {
        if(ShiroUtils.isAdmin()){
            return true;
        }
        for (Object para : paras) {
            if(para != null){
                String permi = permissionService.hasPermi(para.toString());
                if(!PermissionService.NOACCESS.equals(permi)){
                    return true;
                }
            }
        }
        return false;
    }
}
