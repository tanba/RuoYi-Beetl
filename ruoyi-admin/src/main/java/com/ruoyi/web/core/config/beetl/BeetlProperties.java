package com.ruoyi.web.core.config.beetl;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;
import java.util.Properties;

@Component
public class BeetlProperties {

    public boolean isAutocheck = false;

    public Properties getProperties()  {
        Properties properties = new Properties();
        YamlReader reader = null;
        try {
            File file = ResourceUtils.getFile("classpath:application.yml");
            reader = new YamlReader(new FileReader(file));
            Map<String,Object> map = (Map<String, Object>) reader.read();
            Map<String,String> beetlProps = (Map<String, String>) map.get("beetl");
            for(Map.Entry<String,String> entry:beetlProps.entrySet()){
                properties.setProperty(entry.getKey(),entry.getValue());
                if(entry.getKey().toLowerCase().contains("autocheck")){
                    isAutocheck = Boolean.valueOf(entry.getValue());
                }
            }
        } catch (FileNotFoundException | YamlException e) {
            e.printStackTrace();
        }
        return properties;
    }

}
