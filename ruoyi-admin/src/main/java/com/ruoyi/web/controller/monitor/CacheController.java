package com.ruoyi.web.controller.monitor;

import com.ruoyi.common.utils.SimpleMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.framework.web.service.CacheService;

/**
 * 缓存监控
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/monitor/cache")
public class CacheController extends BaseController
{
    private String prefix = "monitor/cache";

    @Autowired
    private CacheService cacheService;

    @GetMapping()
    public String cache(ModelMap mmap)
    {
        mmap.put("cacheNames", cacheService.getCacheNames());
        return prefix + "/cache.html";
    }

    @PostMapping("/getNames")
    @ResponseBody
    public AjaxResult getCacheNames(String fragment, ModelMap mmap)
    {
        return AjaxResult.success(new SimpleMap().set(
                "cacheNames", cacheService.getCacheNames()
        ));
    }

    @PostMapping("/getKeys")
    @ResponseBody
    public AjaxResult getCacheKeys(String fragment, String cacheName, ModelMap mmap)
    {
        return AjaxResult.success(new SimpleMap()
                .set("cacheName", cacheName)
                .set("cacheKeys", cacheService.getCacheKeys(cacheName))
        );
    }

    @PostMapping("/getValue")
    @ResponseBody
    public AjaxResult getCacheValue(String fragment, String cacheName, String cacheKey, ModelMap mmap)
    {
        mmap.put("cacheName", cacheName);
        mmap.put("cacheKey", cacheKey);
        mmap.put("cacheValue", cacheService.getCacheValue(cacheName, cacheKey));
        return AjaxResult.success(new SimpleMap()
                .set("cacheName", cacheName)
                .set("cacheKey", cacheKey)
                .set("cacheValue", cacheService.getCacheValue(cacheName, cacheKey))
        );
    }

    @PostMapping("/clearCacheName")
    @ResponseBody
    public AjaxResult clearCacheName(String cacheName, ModelMap mmap)
    {
        cacheService.clearCacheName(cacheName);
        return AjaxResult.success();
    }

    @PostMapping("/clearCacheKey")
    @ResponseBody
    public AjaxResult clearCacheKey(String cacheName, String cacheKey, ModelMap mmap)
    {
        cacheService.clearCacheKey(cacheName, cacheKey);
        return AjaxResult.success();
    }

    @GetMapping("/clearAll")
    @ResponseBody
    public AjaxResult clearAll(ModelMap mmap)
    {
        cacheService.clearAll();
        return AjaxResult.success();
    }
}
