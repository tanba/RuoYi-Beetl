<script type="text/javascript">
    var ctx = "${staticHome}/";
    var lockscreen = ${session.lockscreen!false};
    if(lockscreen){window.top.location=ctx+"lockscreen";}
</script>
<a id="scroll-up" href="#" class="btn btn-sm display"><i class="fa fa-angle-double-up"></i></a>
<script src="${staticHome}/js/jquery.min.js?v=${jsCssVersion}"></script>
<script src="${staticHome}/js/bootstrap.min.js?v=${jsCssVersion}"></script>
<!-- bootstrap-table 表格插件 -->
<script src="${staticHome}/ajax/libs/bootstrap-table/bootstrap-table.min.js?v=20201129"></script>
<script src="${staticHome}/ajax/libs/bootstrap-table/locale/bootstrap-table-zh-CN.min.js?v=20201129"></script>
<script src="${staticHome}/ajax/libs/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js?v=20201129"></script>
<!-- jquery-validate 表单验证插件 -->
<script src="${staticHome}/ajax/libs/validate/jquery.validate.min.js?v=${jsCssVersion}"></script>
<script src="${staticHome}/ajax/libs/validate/messages_zh.min.js?v=${jsCssVersion}"></script>
<script src="${staticHome}/ajax/libs/validate/jquery.validate.extend.js?v=${jsCssVersion}"></script>
<!-- jquery-validate 表单树插件 -->
<script src="${staticHome}/ajax/libs/bootstrap-treetable/bootstrap-treetable.js?v=${jsCssVersion}"></script>
<!-- 遮罩层 -->
<script src="${staticHome}/ajax/libs/blockUI/jquery.blockUI.js?v=${jsCssVersion}"></script>
<script src="${staticHome}/ajax/libs/iCheck/icheck.min.js?v=${jsCssVersion}"></script>
<script src="${staticHome}/ajax/libs/layer/layer.min.js?v=${jsCssVersion}"></script>
<script src="${staticHome}/ajax/libs/layui/layui.js?v=${jsCssVersion}"></script>
<script src="${staticHome}/ruoyi/js/common.js?v=${jsCssVersion}"></script>
<script src="${staticHome}/ruoyi/js/ry-ui.js?v=${jsCssVersion}"></script>
