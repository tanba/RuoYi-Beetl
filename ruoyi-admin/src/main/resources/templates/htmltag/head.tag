    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>${title!}</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html"/>
    <![endif]-->
    <link href="${staticHome}/favicon.ico?v=${jsCssVersion}" rel="stylesheet"/>
    <link href="${staticHome}/css/bootstrap.min.css?v=${jsCssVersion}" rel="stylesheet"/>
    <link href="${staticHome}/css/jquery.contextMenu.min.css?v=${jsCssVersion}" rel="stylesheet"/>
    <link href="${staticHome}/css/font-awesome.min.css?v=${jsCssVersion}" rel="stylesheet"/>
    <link href="${staticHome}/css/animate.css?v=${jsCssVersion}" rel="stylesheet"/>
    <link href="${staticHome}/css/style.css?v=${jsCssVersion}" rel="stylesheet"/>
    <link href="${staticHome}/css/skins.css?v=${jsCssVersion}" rel="stylesheet"/>
    <link href="${staticHome}/ruoyi/css/ry-ui.css?v=${jsCssVersion}" rel="stylesheet"/>