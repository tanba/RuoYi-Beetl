package com.ruoyi.common.utils;

import java.util.HashMap;

/**
 * @author iterking
 **/
public class SimpleMap<K,V> extends HashMap {

    public SimpleMap<K,V> set(K k,V v){
        super.put(k,v);
        return this;
    }

    public SimpleMap<K,V> set(K k,V v,boolean condition){
        if(condition){
            set(k,v);
        }
        return this;
    }


}
