package com.ruoyi.common.core.bo;

import lombok.Data;

@Data
public class RequestData<T> {
    private String ua = "web";
    private long timestamp;
    private T body;

}
